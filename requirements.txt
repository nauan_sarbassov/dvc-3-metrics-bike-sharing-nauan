dvc==3.19.0
dvclive==2.16.0
gto==1.1.0
ipykernel
joblib==1.3.2
jupyter==1.0.0
matplotlib
pandas==2.1.0
scikit-learn==1.3.0
tqdm==4.66.1

